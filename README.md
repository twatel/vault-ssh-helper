# vault-ssh-helper

This role has goal to setup vault-ssh-helper client : https://github.com/hashicorp/vault-ssh-helper  

## Setup development environment
### Setup environment 
```shell
sudo make install-python
sudo apt-get install direnv -y
if [ ! "$(grep -ir "direnv hook bash" ~/.bashrc)" ];then echo 'eval "$(direnv hook bash)"' >> ~/.bashrc; fi && direnv allow . && source ~/.bashrc
make env prepare
```

## Testing vault-ssh-helper execution in virtual-box environment

```shell
make tests-vbox
```

## Variables
* ``vault_ssh_helper_version``: Version of the client to use  
* ``vault_ssh_helper_dpkg_arch``: dpkg architecture to use  
* ``vault_ssh_helper_dev_mode``: enable dev mode (communicates with Vault with TLS disabled. This is NOT recommended for production use. Use with caution)  
* ``vault_ssh_helper_vault_addr``: [Required] Address of the Vault server.  
* ``vault_ssh_helper_mount_point``: [Required] Mount point of SSH backend in Vault server.  
* ``vault_ssh_helper_namespace``: Namespace of the SSH mount. (Vault Enterprise only)  
* ``vault_ssh_helper_ca_cert``: Path of a PEM-encoded CA certificate file used to verify the Vault server's TLS certificate. -dev mode ignores this value.  
* ``vault_ssh_helper_ca_path``: Path to directory of PEM-encoded CA certificate files used to verify the Vault server's TLS certiciate. -dev mode ignores this value.  
* ``vault_ssh_helper_tls_skip_verify``: Skip TLS certificate verification. Use with caution.  
* ``vault_ssh_helper_allowed_roles``: List of comma-separated Vault SSH roles. The OTP verification response from the server will contain the name of the role against which the OTP was issued. Specify which roles are allowed to login using this configuration. Set this to * to allow any role to perform a login.  
* ``vault_ssh_helper_allowed_cidr_list``: List of comma-separated CIDR blocks. If the IP used by the user to connect to the host is different than the address(es) of the host's network interface(s) (for instance, if the address is NAT-ed), then vault-ssh-helper cannot authenticate the IP. In these cases, the IP returned by Vault will be matched with the CIDR blocks in this list. If it matches, the authentication succeeds. (Use with caution)  

## Platforms
```
platforms:
  - name: Debian
    versions:
      - 12
      - 11
  - name: ubuntu
    versions:
      - 22
      - 20
```
